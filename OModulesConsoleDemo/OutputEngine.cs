﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesConsoleDemo
{
    public static class OutputEngine
    {
        public static void OutputClassTreeNode(string className, int level)
        {
            var line1 = "";
            var line2 = "";
            for (int i = 0; i < level; i++)
            {
                line1 += '|';
                line2 += '|';
            }
            line2 += $" {className}";
            Console.WriteLine(line1);
            Console.WriteLine(line2);
        }
    }
}
