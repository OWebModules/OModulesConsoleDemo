﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OModulesConsoleDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            var globals = new Globals();
            var dbReader = new OntologyModDBConnector(globals);

            var classesResult = dbReader.GetDataClasses();

            if (classesResult.GUID == globals.LState_Error.GUID)
            {
                Console.WriteLine("Error while getting the class-list!");
                Environment.Exit(-1);
            }

            var classes = dbReader.Classes1;

            OutputClassTree(classes);
            Console.Read();

        }

        static void OutputClassTree(List<clsOntologyItem> classList, clsOntologyItem parentClass = null, int level = 0)
        {
            var sbOutput = new StringBuilder();
            var classes = new List<clsOntologyItem>();
            if (parentClass == null)
            {
                classes = classList.Where(cls => string.IsNullOrEmpty(cls.GUID_Parent)).ToList();
            }
            else
            {
                classes = classList.Where(cls => cls.GUID_Parent == parentClass.GUID).ToList();
            }

            foreach (var cls in classes)
            {
                OutputEngine.OutputClassTreeNode(cls.Name, level);
                OutputClassTree(classList, cls, level + 1);
            }

        }
        

    }
}
